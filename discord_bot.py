from config import config
from discord_api import DiscordApi
import time
import os
import random

from slack_alerts import SlackAlerts

if __name__ == '__main__':

    try:
        slackWebHook = os.environ.get("SLACK_WEBHOOK")
    except KeyError as e:
        print("Please define the environment variable {key}".format(key=e))
        exit()

    slackAlerts = SlackAlerts(slackWebHook, config["slack"])

    try:
        
        try:
            loginDiscord = os.environ.get("LOGIN_DISCORD")
            passwordDiscord = os.environ.get("PASSWORD_DISCORD")
        except KeyError as e:
            print("Please define the environment variable {key}".format(key=e))
            exit()
        
        discordApi = DiscordApi(config["discord"])
        try:
            discordLoginData = discordApi.login(loginDiscord, passwordDiscord)
        except Exception as e:
            print("An exception occured while logging into discord. Exception: {exception}".format(exception=e))
            slackAlerts.post_message("An exception occured while logging into discord. Exception: {exception}".format(exception=e))

        while(True):

            try:
                discordApi.post_message(discordLoginData["token"], config["discord"]["channel"], config["discord"]["content"])
            except Exception as e:
                print("Discord post message failed. Try to relogin. Exception: {exception}".format(exception=e))
                discordLoginData = discordApi.login(loginDiscord, passwordDiscord)
                try:
                    discordApi.post_message(discordLoginData["token"], config["discord"]["channel"], config["discord"]["content"])
                except Exception as e:
                    print("Discord post message failed. Exception: {exception}".format(exception=e))
                    slackAlerts.post_message("Discord post message failed. Exception: {exception}".format(exception=e))

            #add random time against system protection
            time.sleep(86400 + 3600 * random.random())

    except Exception as e:
        print("Bot was down with Err: {err}".format(err=e))
        slackAlerts.post_message("Bot was down with Err: {err}".format(err=e))
        exit()
