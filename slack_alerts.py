import requests

class SlackAlerts:

    def __init__(self, incomingWebHook, templateMessage: dict):
        if(incomingWebHook is None):
            raise Exception("Incoming webhook should be set")
            
        self._incomingWebHook = incomingWebHook
        self._templateMessage = templateMessage

    def post_message(self, message):
        headers = {
            "Content-type": "application/json"
        }

        data = {
            "blocks": [
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": self._templateMessage["title"]
                    }
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": "Message: " + message
                    }
                }
            ]
        }

        result = requests.post(self._incomingWebHook, json=data, headers=headers)

        if(result.status_code != 200):
            print("An exception occured while sending alert. StatusCode: {statusCode}, Reason: {reason}, Body: {body}".format(
                statusCode=result.status_code,
                reason=result.reason,
                body=result.json()
            ))