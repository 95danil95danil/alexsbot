import requests
import json

class DiscordApi:
    _scheme = "https"
    _host = "discord.com"
    _apiPrefix = "api"
    _apiVersion = "v9"
    _loginRoute = "auth/login"
    _channel = "channels/{channelId}"
    _messages = "messages"

    def __init__(self, config: dict) -> None:
        self._config = config
        pass

    def get_api_route(self, route):
        return "{scheme}://{host}/{apiPrefix}/{apiVersion}/{route}".format(
            scheme = self._scheme, 
            host = self._host, 
            apiPrefix = self._apiPrefix, 
            apiVersion = self._apiVersion, 
            route = route
        )

    def login(self, login, password):
        if(login is None or password is None):
            raise Exception("Login and password for discord should be set.")

        headers = self._config["headers"]
        data = {
            "login": login,
            "password": password,
            "undelete": False
        }

        result = requests.post(self.get_api_route(self._loginRoute), json=data, headers=headers)
        
        if(result.status_code != 200):
            raise Exception("An exception occurred when logging into the account. Reason: {reason} Response: {response}".format(reason = result.reason, response=result.json()))
        else:
            print('discord login successful')

        return result.json()
    
    def post_message(self, token, channelId, message):

        headers = self._config["headers"]
        headers["authorization"] = token

        data = {
            "content": message,
            "tts": False   
        }

        result = requests.post(self.get_api_route(self._channel.format(channelId=channelId) + "/" + self._messages), json=data, headers=headers)

        if(result.status_code != 200):
            raise Exception("An exception occurred when posting into the channel {channelId}. Reason {reason} Response: {response}".format(channelId = channelId, reason = result.reason, response=result.json()))
        else:
            print("Message have been posted in channel {channelId}".format(channelId = channelId))

    
