#!/bin/sh

if $1 == "" then
    $1 = $CWD
fi

sudo apt-get update
sudo apt-get install python3.8

# $1 is path to folder with bot
pip install -r $1/requirements.txt
pip install supervisor

$1/supervisord.conf > /etc/supervisord.conf

supervisorctl start all