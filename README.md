# Using the bot at your own risk. 
# Discord-VK-Forum(Bungie)-Bot

## Install
Just download rep `git clone rep`

Next run `install.sh {pathToDirectory}`. Sh file need one arg `pathToDirectory`, you can don't pass this arg if you run this file in bot directory.

### Environment variables

Now you should set environment variables. In windows this do it in "Edit the system environment variables", you can find it in an explore 

Need set next vars:

SLACK_WEBHOOK
LOGIN_DISCORD
PASSWORD_DISCORD

### Configuration

Before we start, we should correct our config (`config.py`)

So, we have 4 sections

- discord
- slack

In discord section you should specify channel id which bot will send message and of course message template

We need slack for notify if bot got exception. So, in this section you can add only title for notification

## How to
### How to control bots
You can see your bots when you run `supervisorctl status`. 
Look at http://supervisord.org/running.html for more info.
